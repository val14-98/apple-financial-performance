import json

import requests
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults


class getRatings(BaseOperator):

    @apply_defaults
    def __init__(self, api_key, **kwargs) -> None:
        self.api_key = api_key
        super().__init__(**kwargs)

    def execute(self, context):
        ratings = requests.get(f'https://financialmodelingprep.com/api/v3/rating/AAPL?apikey={self.api_key}').json()[0]

        with open('/tmp/ratings.json', 'w') as f:
            json.dump(ratings, f)
