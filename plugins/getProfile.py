import json

import requests
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults


class getProfile(BaseOperator):

    @apply_defaults
    def __init__(self, api_key, **kwargs) -> None:
        self.api_key = api_key
        super().__init__(**kwargs)

    def execute(self, context):
        profile = requests.get(f'https://financialmodelingprep.com/api/v3/profile/AAPL?apikey={self.api_key}').json()[0]

        with open('/tmp/profile.json', 'w') as f:
            json.dump(profile, f)
