import json
from typing import List, Optional, Union

# from airflow.providers.amazon.aws.hooks.s3 import S3Hook
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults
from airflow.plugins_manager import AirflowPlugin
from pymongo import MongoClient


class ratingMongo(BaseOperator):
    template_fields = ()
    template_ext = ()
    ui_color = '#ededed'

    @apply_defaults
    def __init__(self, mongoserver: str, mongouser: str, mongopass: str, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.mongoserver = mongoserver
        self.mongouser = mongouser
        self.mongopass = mongopass

    def execute(self, context):
        with open('/tmp/ratings.json', 'r') as f:
            data = json.load(f)

        mongo_client = MongoClient(f"mongodb://{self.mongouser}:{self.mongopass}@{self.mongoserver}:27017/")
        db = mongo_client["db"]
        collection = db["financial"]

        x = collection.insert_one(data)
        print(x.inserted_id)
        print(data)


class ratingsPlugin(AirflowPlugin):
    name = "ratingsPlugin"
    operators = [ratingMongo]
