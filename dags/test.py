import airflow
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from datetime import timedelta

from getProfile import getProfile
from getRatings import getRatings
from profileMongo import profileMongo
from ratingMongo import ratingMongo

api_key = "834a114580d75bb98f92d3216acad94a"

args = {
    'id': 2,
    'owner': 'valentin',
    'start_date': airflow.utils.dates.days_ago(2),
}

dag = DAG(
    'starter',
    default_args=args,
    schedule_interval=timedelta(minutes=1),
    catchup=False
)

Get_profile = getProfile(
    task_id='retrieve_profile',
    api_key=api_key,
    dag=dag
)

Get_ratings = getRatings(
    task_id='retrieve_ratings',
    api_key=api_key,
    dag=dag
)

Mongo_profile = profileMongo(
    task_id='mongo_profile',
    mongoserver='mongo',
    mongopass='root',
    mongouser='root',
    dag=dag
)

Mongo_ratings = ratingMongo(
    task_id='mongo_ratings',
    mongoserver='mongo',
    mongopass='root',
    mongouser='root',
    dag=dag
)

Get_profile >> Mongo_profile
Get_ratings >> Mongo_ratings
